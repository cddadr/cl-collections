(asdf:defsystem #:collections
  :class :package-inferred-system
  :pathname "src/"
  :in-order-to ((test-op (load-op #:collections/test/all)))
  :depends-on (#:collections/package))

(asdf:defsystem #:collections/test
  :class :package-inferred-system
  :pathname "src/test"
  :perform (test-op (o c) (symbol-call :rove '#:run c))
  :depends-on (#:rove
               #:collections/test/builtin))
