(defpackage #:collections/test/builtin
  (:use #:cl #:rove))
(in-package #:collections/test/builtin)

(deftest built-in-types
  (testing "Iterate `list"
    (let ((lst (loop for i from 0 below 1000000 collect i)))
      (flet ((iter () (loop for i in lst sum i)))
        (time (iter)))))

  (testing "Iterate `simple-array"
    (let ((arr (make-array 1000000 :element-type 'integer)))
      (loop for i from 0 below 1000000 do (setf (aref arr i) i))
      (flet ((iter () (loop for i across arr sum i)))
        (time (iter)))))

  (testing "Iterate `vector"
    (let ((arr (make-array 1000000 :element-type 'integer :fill-pointer 0 :adjustable t)))
      (loop for i from 0 below 1000000 do (setf (aref arr i) i))
      (flet ((iter () (loop for i across arr sum i)))
        (time (iter)))))

  (testing "Push item `list"
    (let ((lst (list)))
      (flet ((iter () (loop for i from 0 below 1000000 do (push i lst))))
        (time (iter)))))

  (testing "Push item `vector"
    (let ((arr (make-array 0 :element-type 'integer :fill-pointer 0 :adjustable t)))
      (flet ((iter () (loop for i from 0 below 1000000 do (vector-push-extend i arr))))
        (time (iter)))))

  (testing "Pop item `list"
    (let ((lst (loop for i from 0 below 1000000 collect i)))
      (flet ((iter () (loop for i from 0 below 1000000 do (pop lst))))
        (time (iter)))))

  (testing "Pop item `vector"
    (let ((arr (make-array 1000000 :element-type 'integer :fill-pointer 1000000 :adjustable t)))
      (flet ((iter () (loop for i from 0 below 1000000 do (vector-pop arr))))
        (time (iter))))))

  ;; (testing "Remove item randomly `list"
  ;;   (let ((lst (loop for i from 0 below 1000000 collect i)))
  ;;     (labels ((remove-nth (i lst)
  ;;                (let ((v nil))
  ;;                  (if (zerop i)
  ;;                      (setf v (pop lst))
  ;;                      (let ((last (nthcdr (1- i) lst)))
  ;;                        (setf v (cadr last)
  ;;                              (cdr last) (cddr last))))
  ;;                  (values lst v)))
  ;;              (iter () (loop for i from 1000000 to 1
  ;;                             sum (multiple-value-bind (nlst v)
  ;;                                     (remove-nth (random i) lst)
  ;;                                   (setf lst nlst)
  ;;                                   v))
  ;;                       (assert (= 0 (length lst)))))
  ;;       (time (iter))))))

  ;; (testing "Remove item randomly `vector"
  ;;   (let ((arr (make-array 1000000 :element-type 'integer :fill-pointer 1000000 :adjustable t)))
  ;;     (flet ((iter () (loop for i from 0 below 1000000 do (vector-pop arr))))
  ;;       (time (iter))))))
