# WIP: Data Structures Collection for Common Lisp

CL implementations of "Open Data Structures."

## TODO

- [ ] Measure performances of built-in data structures (list, alist, plist, simple-array, vector, hash-table)
- [ ] `array-stack`
- [ ] `array-queue`
- [ ] `array-deque`
- [ ] `dual-array-deque`
- [ ] `rootish-array-stack`
- [ ] `single-linked-list`
- [ ] `double-linked-list`
- [ ] `space-efficient-list`
- [ ] `skip-list-sorted-set`
- [ ] `skip-list-list`
- [ ] `chained-hash-table`
- [ ] `linear-hash-table`
- [ ] `binary-tree`
- [ ] `binary-search-tree`
- [ ] `random-binary-search-tree`
- [ ] `treap`
- [ ] `scapegoat-tree`
- [ ] `2-4-tree`
- [ ] `red-black-tree`
- [ ] `binary-heap`
- [ ] `meldable-heap`
- [ ] `adjacency-matrix`
- [ ] `adjacency-lists`
- [ ] `binary-trie`
- [ ] `x-fast-trie`
- [ ] `y-fast-trie`
- [ ] `block-store`
- [ ] `b-tree`

